package com.geoinfo.asmasyakirah.firstrun;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    final String PREFS_NAME = "MyPrefsFile";
    final String PREFS_FIRST_RUN = "first_run";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        checkFirstRun();
    }

    private void checkFirstRun() {

        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        // If the app is launched for first time, view splash screen and setup 'Next >>' link.
        if (sharedPreferences.getBoolean(PREFS_FIRST_RUN, true)) {

            // Record that user have done first run.
            sharedPreferences.edit().putBoolean(PREFS_FIRST_RUN, false).apply();

            // Setup on click listener to 'Next >>' text view.
            TextView nextLink = (TextView) findViewById(R.id.nextTextView);

            nextLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    goToRegistration();
                }
            });
        }
        // Else, directly go to Registration page.
        else {

            goToRegistration();
        }
    }

    // Go to Registration page.
    public void goToRegistration() {

        Intent intent = new Intent(this, Registration.class);
        startActivity(intent);
        finish();
    }
}
